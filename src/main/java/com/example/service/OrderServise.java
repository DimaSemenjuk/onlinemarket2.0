package com.example.service;

import com.example.entity.Nomenclature;
import com.example.entity.Order;
import com.example.entity.OrderDetail;
import com.example.model.*;
import com.example.pagination.PaginationResult;
import com.example.repos.NomenclatureRepo;
import com.example.repos.OrdRepo;
import com.example.repos.OrderDetalRepository;
import org.hibernate.query.Query;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class OrderServise {

    private final NomenclatureRepo nomenclatureRepo;
    private final OrderDetalRepository orderDetalRepository;
    private final OrdRepo ordRepo;

    public OrderServise(NomenclatureRepo nomenclatureRepo, OrderDetalRepository orderDetalRepository, OrdRepo ordRepo) {
        this.nomenclatureRepo = nomenclatureRepo;
        this.orderDetalRepository = orderDetalRepository;
        this.ordRepo = ordRepo;
    }

    private int getMaxOrderNum() {
        Integer value = ordRepo.getMaxOrderNum();
        if (value == null) {
            return 0;
        }
        return value;
    }

    public void saveOrder(CartInfo cartInfo) {

        int orderNum = this.getMaxOrderNum() + 1;
        Order order = new Order();
        order.setId(UUID.randomUUID().variant());
        order.setOrderNum(orderNum);
        order.setOrderDate(new Date());
        order.setAmount(cartInfo.getAmountTotal());

        CustomerInfo customerInfo = cartInfo.getCustomerInfo();
        order.setuserName(customerInfo.getName());
        order.setCustomerEmail(customerInfo.getEmail());
        //try save order
        ordRepo.save(order);

        List<CartLineInfo> lines = cartInfo.getCartLines();
        for (CartLineInfo line : lines) {
            OrderDetail detail = new OrderDetail();
            detail.setId(UUID.randomUUID().variant());
            detail.setOrder(order);
            detail.setAmount(line.getAmount());
            detail.setPrice(line.getNomenInfo().getPrice());
            detail.setQuanity(line.getQuantity());
            long id = line.getNomenInfo().getId();
            Nomenclature nomenclature;
            nomenclature = nomenclatureRepo.findById(id).get();
            detail.setNomenclature(nomenclature);

            orderDetalRepository.save(detail);
        }

        // Order Number!
        cartInfo.setOrderNum(orderNum);
        // Flush
        //order.flush();
    }

    // @page = 1, 2, ... rebuild (I)
    public PaginationResult<OrderInfo> listOrderInfo(int page, int maxResult, int maxNavigationPage) {
        return ordRepo.findIdDateOrderNumAmount(page,maxResult, maxNavigationPage);
    }
    
    public Order findOrder(long orderId) {
        Order order;
        try{
            order = ordRepo.findById(orderId);
        }catch (NullPointerException e){
            return null;
        }
        return order;
    }

    public OrderInfo getOrderInfo(long orderId) {
        Order order = this.findOrder(orderId);
        if (order == null) {
            return null;
        }
        return new OrderInfo(order.getId(), order.getOrderDate(), //
                order.getOrderNum(), order.getAmount(), order.getUserName());
    }

    public List<OrderDetailInfo> listOrderDetailInfos(Long orderId) {
        return orderDetalRepository.getOrderDetailInfo(orderId);
    }
}