package com.example.service;

import com.example.entity.Role;
import com.example.entity.UserSt;
import com.example.form.Registration;
import com.example.repos.UserStRepo;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

@Service
public class UserStService implements UserDetailsService {

    private final UserStRepo userStRepo;
    private final BCryptPasswordEncoder passwordEncoder;

    public UserStService(UserStRepo userStRepo, BCryptPasswordEncoder passwordEncoder) {
        this.userStRepo = userStRepo;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserSt user = userStRepo.findByEmail(email);
        if (user == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getUserName(),
                user.getPassword(),
                mapRolesToAuthorities(user.getRoles()));
    }

    public UserSt findByEmail(String email){
        return userStRepo.findByEmail(email);
    }

    public UserSt save(Registration registration){
        UserSt user = new UserSt();
        user.setUserName(registration.getUserName());
        user.setEmail(registration.getEmail());
        user.setPassword(passwordEncoder.encode(registration.getPassword()));
        user.setRoles(Collections.singleton(new Role("USER")));
       // System.out.println("Object is hier: "+ user.toString());
        return userStRepo.save(user);
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles){
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }
}