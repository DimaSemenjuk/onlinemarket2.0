package com.example.service;

import com.example.entity.Nomenclature;
import com.example.form.NomenForm;
import com.example.model.NomenInfo;
import com.example.repos.NomenclatureRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

import java.io.IOException;
import java.util.Date;

@Service
public class NomService {

    @Autowired
    private NomenclatureRepo nomenclatureRepo;

    public Nomenclature findNomen(long id) {
        try {
            //session = this.sessionFactory.getCurrentSession();
            return nomenclatureRepo.findById(id).get();

        } catch (NoResultException | NullPointerException e) {
            return null;
        }
    }

    public NomenInfo findNomenInfo(Long id) {
        Nomenclature nomenclature = this.findNomen(id);
        if (nomenclature == null) {
            return null;
        }
        return new NomenInfo(nomenclature.getId(), nomenclature.getName(), nomenclature.getPrice());
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void save(NomenForm nomenForm) {

        //Session session = this.sessionFactory.getCurrentSession();
        Long id = nomenForm.getId();

        Nomenclature nomenclature = null;

        boolean isNew = false;
        if (id != null) {
            nomenclature = this.findNomen(id);
        }
        if (nomenclature == null) {
            isNew = true;
            nomenclature = new Nomenclature();
            nomenclature.setCreateDate(new Date());
        }
        nomenclature.setId(id);
        nomenclature.setName(nomenForm.getName());
        nomenclature.setPrice(nomenForm.getPrice());

        if (nomenForm.getFileData() != null) {
            byte[] image = null;
            try {
                image = nomenForm.getFileData().getBytes();
            } catch (IOException e) {
            }
            if (image != null && image.length > 0) {
                nomenclature.setImage(image);
            }
        }
        if (isNew) {
            nomenclatureRepo.save(nomenclature);
        }
        // If error in DB, Exceptions will be thrown out immediately
        // session.flush();
    }

    public Page<NomenInfo> queryProducts(Pageable pageable, String likeName) {

        if (likeName != null){
            return nomenclatureRepo.findAllByNameOrderByNameDesc(likeName, pageable);
        }else{
            return nomenclatureRepo.getNomenInfoNamePrice(pageable);
        }
    }
/*
    public PaginationResult<NomenInfo> queryProducts(int page, int maxResult, int maxNavigationPage) {
        return queryProducts(page, maxResult, maxNavigationPage, null);
    }
 */
}