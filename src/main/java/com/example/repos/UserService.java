package com.example.repos;

import com.example.entity.UserSt;
import com.example.form.Registration;
import org.springframework.data.repository.CrudRepository;

public interface UserService extends CrudRepository<UserSt, Integer> {
    UserSt findByEmail(String userStService);
    UserSt save(Registration registration);
}