package com.example.repos;

import com.example.entity.OrderDetail;
import com.example.model.OrderDetailInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDetalRepository extends JpaRepository<OrderDetail, Long>{
    //tanks to Aneas
    @Query("select new com.example.model.OrderDetailInfo(d.id, d.nomenclature.name, d.nomenclature.name, " +
            "d.quanity, d.price, d.amount) " +
            "from OrderDetail d where d.order.id = ?1")
    List<OrderDetailInfo> getOrderDetailInfo(Long id);
}