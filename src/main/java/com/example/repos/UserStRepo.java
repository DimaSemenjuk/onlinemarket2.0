package com.example.repos;


import com.example.entity.UserSt;
import com.example.form.Registration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserStRepo extends JpaRepository<UserSt, Long> {
    UserSt findByEmail(String email);
    //Registration findByEmail(String email);
    //UserSt save();
}
