package com.example.repos;

import com.example.entity.Nomenclature;
import com.example.model.NomenInfo;
import com.example.pagination.PaginationResult;
import org.hibernate.annotations.Parameter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NomenclatureRepo extends JpaRepository<Nomenclature, Long> {
    Page<Nomenclature> findAll(Pageable pageable);
    Page<Nomenclature> findByName(String name, Pageable pageable);
    Page<Nomenclature> findByPrice(int price, Pageable pageable);
    List<Nomenclature> save(List<Nomenclature> nomen);

    Nomenclature findById(int code);

    @Query("select new com.example.model.NomenInfo(p.id, p.name, p.price) from Nomenclature p order by p.createDate desc")
    Page<NomenInfo> getNomenInfoNamePrice(Pageable pageable);

    //@Query("select new com.example.model.NomenInfo(p.id, p.name, p.price) from Nomenclature p where lower(p.name) = ?1 order by p.createDate desc")
    Page<NomenInfo> findAllByNameOrderByNameDesc(String name, Pageable pageable);
//    where lower(p.name) like : likename
}