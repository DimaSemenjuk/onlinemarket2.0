package com.example.repos;

import com.example.entity.Order;
import com.example.model.OrderInfo;
import com.example.pagination.PaginationResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdRepo extends JpaRepository<Order, Long> {
    @Query("select max(t.orderNum) from Order t")
    Integer getMaxOrderNum();
    Order findById(long id);
    @Query("select new com.example.model.OrderInfo(ord.id, ord.orderDate, ord.orderNum, ord.amount, ord.customerEmail) from Order ord order by ord.orderNum desc")
    PaginationResult<OrderInfo> findIdDateOrderNumAmount(int page, int maxResult, int maxNavigationPage);
}
