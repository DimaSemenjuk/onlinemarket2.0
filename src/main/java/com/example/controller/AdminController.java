package com.example.controller;

import com.example.entity.Nomenclature;
import com.example.form.NomenForm;
import com.example.model.OrderDetailInfo;
import com.example.model.OrderInfo;
import com.example.pagination.PaginationResult;
import com.example.service.NomService;
import com.example.service.OrderServise;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class AdminController {

    private NomService nomService;
    private OrderServise orderServise;

/*
    @InitBinder
    public void myInitBinder(WebDataBinder dataBinder) {
        Object target = dataBinder.getTarget();
        if (target == null) {
            return;
        }
        System.out.println("Target=" + target);

        if (target.getClass() == NomenForm.class) {
            dataBinder.setValidator(nomenFormValidator);
        }

    */
    // GET: Show Login Page
    @RequestMapping(value = { "/login" }, method = RequestMethod.GET)
    public String login(Model model) {
        return "login";
    }
    /*
    @RequestMapping(value = { "/accountInfo" }, method = RequestMethod.GET)
    public String accountInfo(Model model) {

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        System.out.println(userDetails.getPassword());
        System.out.println(userDetails.getUsername());
        System.out.println(userDetails.isEnabled());

        model.addAttribute("userDetails", userDetails);
        return "accountInfo";
    }

    @RequestMapping(value = { "/orderList" }, method = RequestMethod.GET)
    public String orderList(Model model, //
                            @RequestParam(value = "page", defaultValue = "1") String pageStr) {
        int page = 1;
        try {
            page = Integer.parseInt(pageStr);
        } catch (Exception e) {
        }
        final int MAX_RESULT = 5;
        final int MAX_NAVIGATION_PAGE = 10;
        PaginationResult<OrderInfo> paginationResult //
                = orderServise.listOrderInfo(page, MAX_RESULT, MAX_NAVIGATION_PAGE);
        model.addAttribute("paginationResult", paginationResult);
        return "orderList";
    }

    // GET: Show product.
    @RequestMapping(value = { "/nomen" }, method = RequestMethod.GET)
    public String nomen(Model model, @RequestParam(value = "id", defaultValue = "") long id) {
        NomenForm nomenForm = null;
        if ( id > 0) {
            Nomenclature nomenclature = nomService.findNomen(id);
            if (nomenclature != null) {
                nomenForm = new NomenForm(nomenclature);
            }
        }
        if (nomenForm == null) {
            nomenForm = new NomenForm();
            nomenForm.setNewProduct(true);
        }
        model.addAttribute("nomenForm", nomenForm);
        return "nomen";
    }

    // POST: Save nomen
    @RequestMapping(value = { "/nomen" }, method = RequestMethod.POST)
    public String nomenSave(Model model, //
                              @ModelAttribute("nomenForm") @Validated NomenForm nomenForm, //
                              BindingResult result, //
                              final RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "nomen";
        }
        try {
            nomService.save(nomenForm);
        } catch (Exception e) {
            Throwable rootCause = ExceptionUtils.getRootCause(e);
            String message = rootCause.getMessage();
            model.addAttribute("errorMessage", message);
            // Show nomen form.
            return "nomen";
        }

        return "redirect:/nomenList";
    }

    @RequestMapping(value = { "/order" }, method = RequestMethod.GET)
    public String orderView(Model model, @RequestParam("orderId") long orderId) {
        OrderInfo orderInfo = null;
        if (orderId > 0) {
            orderInfo = this.orderServise.getOrderInfo(orderId);
        }
        if (orderInfo == null) {
            return "redirect:/orderList";
        }
        List<OrderDetailInfo> details = this.orderServise.listOrderDetailInfos(orderId);
        orderInfo.setDetails(details);

        model.addAttribute("orderInfo", orderInfo);

        return "order";
    }
*/
}