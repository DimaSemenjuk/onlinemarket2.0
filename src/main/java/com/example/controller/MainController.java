    package com.example.controller;

import com.example.entity.Nomenclature;
import com.example.form.UserForm;
import com.example.model.CartInfo;
import com.example.model.CustomerInfo;
import com.example.model.NomenInfo;
import com.example.pagination.PaginationResult;
import com.example.repos.NomenclatureRepo;
import com.example.service.NomService;
import com.example.service.OrderServise;
import com.example.utils.Utils;
import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class MainController {
    static final Logger log = Logger.getLogger(MainController.class);
    private final NomenclatureRepo nomenclatureRepo;
    private final OrderServise orderServise;
    private final NomService nomService;

    public MainController(NomenclatureRepo nomenclatureRepo, OrderServise orderServise, NomService nomService) {
        this.nomenclatureRepo = nomenclatureRepo;
        this.orderServise = orderServise;
        this.nomService = nomService;
    }

    @GetMapping("/index")
    public String index(){
        return "index";
    }

    /*
    public String index(@PageableDefault(size = 7) Pageable pageable,
                        Model model, Authentication authentication)
    {
        if (authentication != null) {
            log.info("Greetings whose wisited by " + authentication.getName());
        } else{}
        Page<Nomenclature> page = nomenclatureRepo.findAll(pageable);
        model.addAttribute("page", page);
        return "index";
    }

    @PostMapping("/filter")
        public String filter(@PageableDefault(size = 7) Pageable pageable,
                Model model, Authentication authentication,@RequestParam String filter){
            if (authentication != null) {
                log.info("Greetings whose wisited by " + authentication.getName());
            } else {

            }
        Page<Nomenclature> page = null;
        if(filter.isEmpty()){}else{}
        try{
            page = nomenclatureRepo.findByPrice(Integer.parseInt(filter), pageable);
        } catch (NumberFormatException e)
        {
            page = nomenclatureRepo.findByName(filter, pageable);
        } catch (NullPointerException e){
            page = nomenclatureRepo.findAll(pageable);
        } catch (Exception e){
            e.getMessage();
        } finally {
            if(filter.isEmpty()){
               page = nomenclatureRepo.findAll(pageable);
            }
            model.addAttribute("page", page);
        }
        return "index";
    }
*/
/*
    @InitBinder
    public void myInitBinder(WebDataBinder dataBinder) {

        Object target = dataBinder.getTarget();
        if (target == null) {
            return;
        }
        System.out.println("Target=" + target);

        // Case update quantity in cart
        // (@ModelAttribute("cartForm") @Validated CartInfo cartForm)
        if (target.getClass() == CartInfo.class) {
        }
        // Case save customer information.
        // (@ModelAttribute @Validated CustomerInfo customerForm)
       else if (target.getClass() == CustomerForm.class) {
            dataBinder.setValidator(customerFormValidator);
        }

    }

    @RequestMapping("/403")
    public String accessDenied() {
        return "/403";
    }
*/
    @RequestMapping("/")
    public String home() {
        return "index";
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    // Product List
    @RequestMapping({ "/nomenList" })
    public String listProductHandler(@PageableDefault(size = 5) Pageable pageable, Model model, String likename) {
        Page<NomenInfo> paginationNomens = nomService.queryProducts(pageable, likename);
        model.addAttribute("paginationNomens", paginationNomens);
        return "nomenList";
    }

    @RequestMapping({ "/buyNomen" })
    public String listProductHandler(HttpServletRequest request, Model model, //
                                     @RequestParam(value = "id", defaultValue = "") int id) {
        Nomenclature nomenclature = null;
        if ( id > 0 ) {
            nomenclature = nomService.findNomen(id);
        }
        if (nomenclature != null) {
            CartInfo cartInfo = Utils.getCartInSession(request);
            NomenInfo productInfo = new NomenInfo(nomenclature);
            cartInfo.addNomen(productInfo, 1);
        }
        return "redirect:/shoppingCart";
    }

    @RequestMapping({ "/shoppingCartRemoveNomen" })
    public String removeProductHandler(HttpServletRequest request, Model model, //
                                       @RequestParam(value = "id", defaultValue = "") long id) {
        Nomenclature nomenclature = null;
//        if (id != null && id.length() > 0) {
        if( id > 0 ){
            nomenclature = nomService.findNomen(id);
        }
        if(nomenclature != null) {
            CartInfo cartInfo = Utils.getCartInSession(request);
            NomenInfo nomenInfo = new NomenInfo(nomenclature);
            cartInfo.removeProduct(nomenInfo);
        }
        return "redirect:/shoppingCart";
    }


    // POST: Update quantity for product in cart
    @RequestMapping(value = { "/shoppingCart" }, method = RequestMethod.POST)
    public String shoppingCartUpdateQty(HttpServletRequest request, //
                                        Model model, //
                                        @ModelAttribute("cartForm") CartInfo cartForm) {
        CartInfo cartInfo = Utils.getCartInSession(request);
        cartInfo.updateQuantity(cartForm);

        return "redirect:/shoppingCart";
    }

    // GET: Show cart.
    @RequestMapping(value = { "/shoppingCart" }, method = RequestMethod.GET)
    public String shoppingCartHandler(HttpServletRequest request, Model model) {
        CartInfo myCart = Utils.getCartInSession(request);
        model.addAttribute("cartForm", myCart);
        return "shoppingCart";
    }

    // GET: Enter customer information.
    @RequestMapping(value = { "/shoppingCartCustomer" }, method = RequestMethod.GET)
    public String shoppingCartCustomerForm(HttpServletRequest request, Model model) {
        CartInfo cartInfo = Utils.getCartInSession(request);
        if (cartInfo.isEmpty()) {
            return "redirect:/shoppingCart";
        }
        CustomerInfo customerInfo = cartInfo.getCustomerInfo();
        UserForm customerForm = new UserForm(customerInfo);
        model.addAttribute("customerForm", customerForm);
        return "shoppingCartCustomer";
    }

    // POST: Save customer information.
    @RequestMapping(value = { "/shoppingCartCustomer" }, method = RequestMethod.POST)
    public String shoppingCartCustomerSave(HttpServletRequest request, //
                                           Model model, //
                                           @ModelAttribute("customerForm") @Validated UserForm customerForm, //
                                           BindingResult result, //
                                           final RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            customerForm.setValid(false);
            // Forward to reenter customer info.
            return "shoppingCartCustomer";
        }

        customerForm.setValid(true);
        CartInfo cartInfo = Utils.getCartInSession(request);
        CustomerInfo customerInfo = new CustomerInfo(customerForm);
        cartInfo.setCustomerInfo(customerInfo);

        return "redirect:/shoppingCartConfirmation";
    }

    // GET: Show information to confirm.
    @RequestMapping(value = { "/shoppingCartConfirmation" }, method = RequestMethod.GET)
    public String shoppingCartConfirmationReview(HttpServletRequest request, Model model) {
        CartInfo cartInfo = Utils.getCartInSession(request);
        if (cartInfo == null || cartInfo.isEmpty()) {
            return "redirect:/shoppingCart";
        } else if (!cartInfo.isValidCustomer()) {
            return "redirect:/shoppingCartCustomer";
        }
        model.addAttribute("myCart", cartInfo);

        return "shoppingCartConfirmation";
    }

    @PostMapping("/shoppingCartConfirmation")
    public String shoppingCartConfirmationSave(HttpServletRequest request, Model model) {
        CartInfo cartInfo = Utils.getCartInSession(request);
        //OrderServise orderServise = new OrderServise(request);
        if (cartInfo.isEmpty()) {
            return "redirect:/shoppingCart";
        } else if (!cartInfo.isValidCustomer()) {
            return "redirect:/shoppingCartCustomer";
        }
        try {
            orderServise.saveOrder(cartInfo);
        } catch (Exception e) {

            return "shoppingCartConfirmation";
        }
        // Remove Cart from Session.
        Utils.removeCartInSession(request);
        // Store last cart.
        Utils.storeLastOrderedCartInSession(request, cartInfo);
        return "redirect:/shoppingCartFinalize";
    }

    @RequestMapping(value = { "/shoppingCartFinalize" }, method = RequestMethod.GET)
    public String shoppingCartFinalize(HttpServletRequest request, Model model) {
        CartInfo lastOrderedCart = Utils.getLastOrderedCartInSession(request);
        if (lastOrderedCart == null) {
            return "redirect:/shoppingCart";
        }
        model.addAttribute("lastOrderedCart", lastOrderedCart);
        return "shoppingCartFinalize";
    }

    @RequestMapping(value = { "/nomenImage" }, method = RequestMethod.GET)
    public void productImage(HttpServletRequest request, HttpServletResponse response, Model model,
                             @RequestParam("id") int id) throws IOException {
        Nomenclature nomenclature = null;
        if (id > 0) nomenclature = this.nomenclatureRepo.findById(id);
        if (nomenclature != null && nomenclature.getImage() != null) {
            response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
            response.getOutputStream().write(nomenclature.getImage());
        }
        response.getOutputStream().close();
    }
}