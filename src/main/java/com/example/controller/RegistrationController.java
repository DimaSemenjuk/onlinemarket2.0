package com.example.controller;

import com.example.entity.UserSt;
import com.example.form.Registration;
import com.example.service.UserStService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class RegistrationController {
/*
    @Autowired
    private UserRepo userRepo;
*/
    final static Logger logger = Logger.getLogger(RegistrationController.class);

    private final UserStService userStService;

    public RegistrationController(UserStService userStService) {
        this.userStService = userStService;
    }

    @ModelAttribute("user")
    public Registration userRegistrationDto() {
        return new Registration();
    }

    @GetMapping
    public String showRegistrationForm(Model model) {
        return "registration";
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("user") @Valid Registration reguser,
                                      BindingResult result){
        /*UserSt existing = userStService.findByEmail(reguser.getEmail());
        if (existing != null){
            result.rejectValue("email", null, "There is already an account registered with that email");
        }
        if (result.hasErrors()){
            return "registration";
        }
        */

        userStService.save(reguser);
        return "redirect:/registration?success";
    }
}