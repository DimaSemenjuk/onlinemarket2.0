package com.example.form;

import com.example.entity.Nomenclature;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.constraints.NotEmpty;

public class NomenForm {
    @NotEmpty
    private Long id;
    @NotEmpty
    private String name;
    @NotEmpty
    private double price;

    private boolean newNomen = false;
    private MultipartFile fileData;

    public NomenForm() { this.newNomen = true; }

    public NomenForm(Nomenclature nomenclature) {
        this.id = nomenclature.getId();
        this.name = nomenclature.getName();
        this.price = nomenclature.getPrice();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.   name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public MultipartFile getFileData() {
        return fileData;
    }

    public void setFileData(MultipartFile fileData) {
        this.fileData = fileData;
    }

    public boolean isNewNomen() {
        return newNomen;
    }

    public void setNewNomen(boolean newNomen) {
        this.newNomen = newNomen;
    }

}