package com.example.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "nomenclature", schema = "public", catalog = "onlinemarket")//profile_users
public class Nomenclature implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    @NotNull
    private String name;
//    @NotNull
  //  private String description;
    @NotNull
    private double price;
    @Lob
    private byte[] image;

    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date createDate;

    public void setPrice(double price) {
        this.price = price;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {this.createDate = createDate;}

    public Nomenclature() {}
    //long, java.lang.String, int [select new com.example.model.NomenInfo(p.id, p.name, p.price)
    public Nomenclature(long id, String name, double price){
        this.id = id ;
        this.name = name ;
        this.price = price ;
    }
    public long getId() {return id;}
    public void setId(long id) {this.id = id;}
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
/*    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
*/    public double getPrice() {return price;}
}