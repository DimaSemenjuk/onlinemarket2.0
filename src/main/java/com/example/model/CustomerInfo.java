package com.example.model;


import com.example.form.UserForm;

public class CustomerInfo {

    private String name;
    private String email;

    private boolean valid;

    public CustomerInfo() {

    }

    public CustomerInfo(UserForm customerForm) {
        this.name = customerForm.getName();
        this.email = customerForm.getEmail();
        this.valid = customerForm.isValid();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

}