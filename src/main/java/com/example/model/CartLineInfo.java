package com.example.model;

public class CartLineInfo {

    private NomenInfo nomenInfo;
    private int quantity;

    public CartLineInfo() {
        this.quantity = 0;
    }

    public NomenInfo getNomenInfo() {
        return nomenInfo;
    }

    public void setNomenInfo(NomenInfo nomenInfo) {
        this.nomenInfo = nomenInfo;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getAmount() {
        return this.nomenInfo.getPrice() * this.quantity;
    }

}