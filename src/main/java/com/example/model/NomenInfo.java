package com.example.model;

import com.example.entity.Nomenclature;

public class NomenInfo {

    private long id;
    private String name;
    private double price;

    public NomenInfo() {
    }

    public NomenInfo(Nomenclature nomenclature) {
        this.id = nomenclature.getId();
        this.name = nomenclature.getName();
        this.price = nomenclature.getPrice();
    }

    // Using in JPA/Hibernate query
    public NomenInfo(long id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}